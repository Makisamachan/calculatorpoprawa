import java.util.Scanner;

public class CalculatorLauncher {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();

        String op;

        Scanner sc = new Scanner(System.in);
        op = sc.next();

        if(op.contains("+")){
            calculator.addNumber(op);
        }else if(op.contains("-")){
            calculator.substractNumber(op);
        }else if(op.contains("*")){
            calculator.multiplyNumber(op);
        }else if(op.contains("/")){
            calculator.divideNumber(op);
        }else if(op.contains("^")){
            calculator.expNumber(op);
        }
    }
}
