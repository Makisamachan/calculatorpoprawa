public class Calculator {

    static void addNumber(String operation){
        int n1, n2;

        n1 = Integer.valueOf(operation.substring(0,operation.indexOf("+")));
        n2 = Integer.valueOf(operation.substring(operation.indexOf("+")+1));

        System.out.println(n1+n2);
    }

    static void substractNumber(String operation){
        int n1, n2;

        n1 = Integer.valueOf(operation.substring(0,operation.indexOf("-")));
        n2 = Integer.valueOf(operation.substring(operation.indexOf("-")+1));

        System.out.println(n1-n2);
    }

    static void multiplyNumber(String operation){
        int n1, n2;

        n1 = Integer.valueOf(operation.substring(0,operation.indexOf("*")));
        n2 = Integer.valueOf(operation.substring(operation.indexOf("*")+1));

        System.out.println(n1*n2);
    }

    static void divideNumber(String operation){
        int n1, n2;

        n1 = Integer.valueOf(operation.substring(0,operation.indexOf("/")));
        n2 = Integer.valueOf(operation.substring(operation.indexOf("/")+1));

        if (n2 == 0){
            System.out.println("Division by zero is undefined");
        }else{
            System.out.println(n1/n2);
        }
    }

    static void expNumber(String operation){
        int n1, n2;

        n1 = Integer.valueOf(operation.substring(0,operation.indexOf("^")));
        n2 = Integer.valueOf(operation.substring(operation.indexOf("^")+1));

        System.out.println(Math.pow(n1,n2));
    }
}
